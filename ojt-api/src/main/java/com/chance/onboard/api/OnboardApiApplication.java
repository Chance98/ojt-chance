package com.chance.onboard.api;

import com.chance.onboard.configuration.OjtCommonConfig;
import com.chance.onboard.configuration.OjtPostgreConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = { FlywayAutoConfiguration.class })//exclude = { FlywayAutoConfiguration.class }
@Import({OjtCommonConfig.class})
public class OnboardApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(OnboardApiApplication.class, args);
    }
}


