package com.chance.onboard.api.controller;

import com.chance.onboard.domain.center.Center;
import com.chance.onboard.domain.center.CenterService;
import com.chance.onboard.domain.center.dto.CenterDto;
import com.chance.onboard.domain.center.dto.CenterUpdateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CenterController {

//    @Autowired
    private final CenterService centerService;

    public CenterController(CenterService centerService) {
        this.centerService = centerService;
    }

    @GetMapping(value = "/center")
    public List<Center> listAllCenters(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
        PageRequest req = PageRequest.of(page, size);
        Page<Center> result = centerService.listCenter(req);
        return result.getContent();
    }

    @PostMapping(value = "/center")
    public CenterDto createCenter(@RequestBody CenterDto centerDto) {
        return centerService.createCenter(centerDto);
    }

    @PutMapping(value = "/center/{centerId}")
    public CenterDto updateCenter(@PathVariable Long centerId, @RequestBody CenterUpdateDto dto) {
        return centerService.updateCenter(centerId, dto);
    }

    @DeleteMapping(value = "/center/{centerId}")
    public CenterDto deleteCenter(@PathVariable Long centerId) {
        return centerService.deleteCenter(centerId);
    }
}
