INSERT INTO public.center
(center_id, center_name, center_eng_name, center_type, center_postal_code, center_code, center_address, is_active, center_latitude, center_longitude)
VALUES
(nextval('center_center_id_seq'), '한솔CSN 용산물류센터', 'Hansol CSN', 'LOGISTICS_CENTER', '12345', 'CSN1', '한강로3가 40-1', true, 123.231231, 37.1231231),
(nextval('center_center_id_seq'), '더남물류센터', 'TheNam', 'LOGISTICS_CENTER', '32423', 'TN1', '사당동 어딘가', true, 122.3213211, 36.1232342331),
(nextval('center_center_id_seq'), '(주)우아한청년들 물류센터', 'Aesthetic boys', 'LOGISTICS_CENTER', '21352', 'ABC1', '봉천동 남부순환로 1836 지하1층', false, 123.21231231, 36.12312331),
(nextval('center_center_id_seq'), 'LG 물류센터', 'LG center', 'LOGISTICS_CENTER', '21243', 'LG1', '어딘가에 있겠지', true, 123.2132131, 37.177712331),
(nextval('center_center_id_seq'), 'KCTC 양재물류센터', 'KCTC', 'LOGISTICS_CENTER', '36936', 'KCTC1', '양재동 224', true, 122.2123112312, 36.9812312331),
(nextval('center_center_id_seq'), '켄달스퀘어 부천물류센터', 'Kendal', 'DELIVERY_CENTER', '56789', 'KEN2', '경기도 부천시', true, 121.21231231, 37.1231233),
(nextval('center_center_id_seq'), '이마트 물류센터', 'Emart', 'DELIVERY_CENTER', '58292', 'EM2', '해피해피해피해피', true, 122.893498112, 37.12312213123),
(nextval('center_center_id_seq'), '서영물류', 'Seoyong', 'DELIVERY_CENTER', '44433', 'SY2', '당산동1가 12', false, 122.2187231231, 37.23421231233),
(nextval('center_center_id_seq'), '용마로지스 김포물류센터', 'Yongma', 'DELIVERY_CENTER', '56789', 'KEN2', '경기도 부천시', true, 121.21231231, 37.1231233),
(nextval('center_center_id_seq'), '쿠팡 고양물류센터', 'Coupang', 'DELIVERY_CENTER', '00000', 'COU', '경기도 고양시', false, 123.21298898989, 37.192911090),
(nextval('center_center_id_seq'), '온채널물류센터', 'ONChanel', 'LOGISTICS_HUB', '12312', 'CHA1', '경기도 파주시', true, 122.2892389239, 36.19298121090),
(nextval('center_center_id_seq'), '더남물류센터', 'thenam', 'LOGISTICS_HUB', '22222', 'nam1a', '사당동', true, 122.28923839, 36.1929828980),
(nextval('center_center_id_seq'), '농협유통 양재물류센터', 'Nonghyup', 'LOGISTICS_HUB', '30211', 'HYUP1', '양재동 230', true, 122.289349839, 37.1944421090),
(nextval('center_center_id_seq'), '안국물류센터', 'Angook', 'LOGISTICS_HUB', '67677', 'Ahn1', '마곡동 8-1', true, 122.323289239, 36.1454521090),
(nextval('center_center_id_seq'), '서서울물류센터', 'seoseoul', 'LOGISTICS_HUB', '11231', 'SEO1', '목동', true, 124.2892389239, 37.19298121090)



