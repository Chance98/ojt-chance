CREATE TABLE "center" (
       "center_id" serial not null,
       "center_name" varchar(300) not null,
       "center_eng_name" varchar(300) not null,
       "center_type" varchar(50) not null,
       "center_postal_code" varchar(20) not null,
       "center_code" varchar(20) not null,
       "center_address" text not null,
       "is_active" boolean default false,
       "center_latitude" numeric(16, 13),
       "center_longitude" numeric(16, 13),
       "created_at" timestamp default CURRENT_TIMESTAMP not null,
       "updated_at" timestamp default CURRENT_TIMESTAMP not null,
       PRIMARY KEY(center_id)
);

