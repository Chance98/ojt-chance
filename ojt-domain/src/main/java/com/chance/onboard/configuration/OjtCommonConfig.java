package com.chance.onboard.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(OjtPostgreConfig.class)
@EntityScan(basePackages = {"com.chance.onboard"})
@ComponentScan(basePackages = {"com.chance.onboard"})
public class OjtCommonConfig {


}
