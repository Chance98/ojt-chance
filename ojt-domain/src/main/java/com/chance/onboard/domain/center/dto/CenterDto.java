package com.chance.onboard.domain.center.dto;

import com.chance.onboard.domain.center.Center;
import com.chance.onboard.domain.enums.CenterType;
import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CenterDto {
    private String centerName;
    private String centerEngName;
    private String centerCode;
    private String centerPostalCode;
    private String centerAddress;
    private CenterType centerType;
    private Boolean isActive;
    private Double centerLatitude;
    private Double centerLongitude;

    public static CenterDto from(Center center) {
        return builder()
                .centerName(center.getCenterName())
                .centerEngName(center.getCenterEngName())
                .centerCode(center.getCenterCode())
                .centerPostalCode(center.getCenterPostalCode())
                .centerAddress(center.getCenterAddress())
                .centerType(center.getCenterType())
                .isActive(center.getIsActive())
                .centerLatitude(center.getCenterLatitude())
                .centerLongitude(center.getCenterLongitude())
                .build();
    }
}
