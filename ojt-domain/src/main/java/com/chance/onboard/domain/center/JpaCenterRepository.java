package com.chance.onboard.domain.center;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface JpaCenterRepository extends JpaRepository<Center, Long>, CenterRepository {
}
