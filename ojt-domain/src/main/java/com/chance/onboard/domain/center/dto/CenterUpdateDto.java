package com.chance.onboard.domain.center.dto;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CenterUpdateDto {
    private String centerName;
    private String centerEngName;
    private String centerPostalCode;
    private String centerAddress;
    private String centerLatitude;
    private String centerLongitude;
}
