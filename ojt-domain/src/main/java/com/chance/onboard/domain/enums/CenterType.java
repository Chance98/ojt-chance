package com.chance.onboard.domain.enums;

public enum CenterType {
    DELIVERY_CENTER, LOGISTICS_CENTER, LOGISTICS_HUB
}
