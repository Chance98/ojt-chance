package com.chance.onboard.domain.center;

import com.chance.onboard.domain.center.dto.CenterDto;
import com.chance.onboard.domain.enums.CenterType;
import lombok.*;

import javax.persistence.*;


@Entity
@Data
@Builder
@Table(name = "center", schema = "public")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Center {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(columnDefinition="serial", name="center_id")
    private Long centerId;

    @Column(name="center_name")
    private String centerName;

    @Column(name="center_eng_name")
    private String centerEngName;

    @Column(unique=true, name="center_code")
    private String centerCode;

    @Column(name="center_address")
    private String centerAddress;

    @Column(name="center_postal_code")
    private String centerPostalCode;

    @Column(name="center_type")
    @Enumerated(EnumType.STRING)
    private CenterType centerType;

    @Column(name="is_active")
    private Boolean isActive;

    @Column(name="center_latitude")
    private Double centerLatitude;

    @Column(name="center_longitude")
    private Double centerLongitude;

    public static Center from(CenterDto dto) {
        return Center.builder()
                .centerName(dto.getCenterName())
                .centerEngName(dto.getCenterEngName())
                .centerCode(dto.getCenterCode())
                .centerPostalCode(dto.getCenterPostalCode())
                .centerType(dto.getCenterType())
                .centerAddress(dto.getCenterAddress())
                .isActive(dto.getIsActive())
                .centerLatitude(dto.getCenterLatitude())
                .centerLongitude(dto.getCenterLongitude())
                .build();
    }


}
