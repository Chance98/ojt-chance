package com.chance.onboard.domain.center;

import com.chance.onboard.domain.center.Center;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CenterRepository extends JpaRepository<Center, Long> {
    //Center save(Center center);

    Page<Center> findByIsActiveIsTrue(Pageable pageable);

    Center findByCenterId(Long centerId);
}
