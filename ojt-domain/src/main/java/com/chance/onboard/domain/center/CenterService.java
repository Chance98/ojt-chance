package com.chance.onboard.domain.center;

import com.chance.onboard.common.exception.OjtException;
import com.chance.onboard.common.utils.CopyUtil;
import com.chance.onboard.domain.center.dto.CenterDto;
import com.chance.onboard.domain.center.dto.CenterUpdateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.chance.onboard.common.Messages.ExceptionError.NOT_FOUND;

@RequiredArgsConstructor
@Service
public class CenterService {

    private final CenterRepository centerRepository;

    public Page<Center> listCenter(Pageable paging) {
        return centerRepository.findByIsActiveIsTrue(paging);
    }

    public CenterDto createCenter(CenterDto centerDto) {
        Center center = Center.from(centerDto);
        return CenterDto.from(centerRepository.save(center));
    }

    public CenterDto updateCenter(Long centerId, CenterUpdateDto dto) {
        Center center = centerRepository.findById(centerId)
                .orElseThrow(new OjtException(NOT_FOUND));
        CopyUtil.copyNonNullProperties(dto, center);
        return CenterDto.from(centerRepository.save(center));
    }

    public CenterDto deleteCenter(Long centerId) {
        Center center = centerRepository.findByCenterId(centerId);
        centerRepository.delete(center);
        return CenterDto.from(center);
    }

}
