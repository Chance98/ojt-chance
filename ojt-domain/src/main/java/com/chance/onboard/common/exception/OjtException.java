package com.chance.onboard.common.exception;

import java.util.function.Supplier;

public class OjtException extends RuntimeException implements Supplier<RuntimeException> {

    public OjtException(String message) {
        super(message);
    }

    @Override
    public RuntimeException get() {
        return this;
    }
}
