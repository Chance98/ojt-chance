package com.chance.onboard.common;

public final class Messages {
    public static final class ExceptionError {
        public static final String NOT_FOUND = "Could not find the requested resource.";
    }
}
